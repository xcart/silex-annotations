<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Annotations\Router;

use XCart\SilexAnnotationsTest\RoutesAnnotationsTestBase;

class AssertTest extends RoutesAnnotationsTestBase
{
    public function testAssert()
    {
        $this->assertEndPointStatus(self::GET_METHOD, '/test/assert/45', self::STATUS_OK);
        $this->assertEndPointStatus(self::GET_METHOD, '/test/assert/fail', self::STATUS_NOT_FOUND);
    }

    public function testAssertCollection()
    {
        $this->assertEndPointStatus(self::GET_METHOD, '/assert/test/45', self::STATUS_OK);
        $this->assertEndPointStatus(self::GET_METHOD, '/assert/test/fail', self::STATUS_NOT_FOUND);
    }
}
