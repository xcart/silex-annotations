<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Fixtures\AnnotationsSample;

use ReflectionClass;
use Silex\Application;

/**
 * @Annotation
 * @Target("CLASS")
 */
class SomeAnnotation
{
    public $name;

    public function process(Application $app, ReflectionClass $reflectionClass)
    {}
}
