<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Annotations\Router;

use XCart\SilexAnnotationsTest\RoutesAnnotationsTestBase;

class BindTest extends RoutesAnnotationsTestBase
{
    public function testBind()
    {
        $response = $this->makeRequest(self::GET_METHOD, '/test/bind');
        $this->assertEquals('/test/bind', $response->getContent());
    }
}

