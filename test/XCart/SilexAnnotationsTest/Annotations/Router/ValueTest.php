<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Annotations\Router;

use XCart\SilexAnnotationsTest\RoutesAnnotationsTestBase;

class ValueTest extends RoutesAnnotationsTestBase
{
    public function testDefaultValue()
    {
        $this->assertEndPointStatus(self::GET_METHOD, '/foo', self::STATUS_OK);

        $response = $this->makeRequest(self::GET_METHOD, '/');
        $this->assertStatus($response, self::STATUS_OK);
        $this->assertEquals('default', $response->getContent());
    }
}
