<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Fixtures\ServiceAnnotationSample;

use Silex\Application;
use XCart\SilexAnnotations\Annotations\Service;

/**
 * @Service\Factory()
 */
class SimpleFactory {
    public $app;

    /**
     * @Service\Constructor()
     */
    public static function serviceConstructor(Application $app)
    {
        $instance = new self;
        $instance->app = $app;

        return $instance;
    }
}
