<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Fixtures\AnnotationsSample\Sample;

use XCart\SilexAnnotationsTest\Fixtures\AnnotationsSample\SomeAnnotation;

/**
 * @SomeAnnotation
 */
class Sample
{
}
