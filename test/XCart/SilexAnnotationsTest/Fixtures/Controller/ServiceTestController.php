<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Fixtures\Controller;

use Symfony\Component\HttpFoundation\Response;
use XCart\SilexAnnotations\Annotations\Router;
use XCart\SilexAnnotations\Annotations\Service;

/**
 * @Service\Service()
 * @Router\Controller(prefix="/")
 */
class ServiceTestController
{
    /**
     * @Router\Request(method="GET", uri="/service")
     */
    public function testMethod()
    {
        return new Response(self::class);
    }
}
