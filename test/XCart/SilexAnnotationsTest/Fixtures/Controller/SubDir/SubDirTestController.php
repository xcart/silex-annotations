<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Fixtures\Controller\SubDir;

use XCart\SilexAnnotations\Annotations\Router;

/**
 * @Router\Controller
 */
class SubDirTestController 
{

} 