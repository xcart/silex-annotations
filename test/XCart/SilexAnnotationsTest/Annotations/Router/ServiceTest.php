<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Annotations\Router;

use XCart\SilexAnnotationsTest\Fixtures\Controller\ServiceTestController;
use XCart\SilexAnnotationsTest\RoutesAnnotationsTestBase;

class ServiceTest extends RoutesAnnotationsTestBase
{
    public function testDefaultValue()
    {
        $response = $this->makeRequest(self::GET_METHOD, '/service');
        $this->assertStatus($response, self::STATUS_OK);
        $this->assertEquals(ServiceTestController::class, $response->getContent());
    }
}
