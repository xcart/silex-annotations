<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Annotations\Router;

use XCart\SilexAnnotationsTest\RoutesAnnotationsTestBase;

class ConvertTest extends RoutesAnnotationsTestBase
{
    public function testConvert()
    {
        $response = $this->makeRequest(self::GET_METHOD, '/test/convert/45');
        $this->assertStatus($response, self::STATUS_OK);
        $this->assertEquals('50', $response->getContent());
    }

    public function testConvertCollection()
    {
        $response = $this->makeRequest(self::GET_METHOD, '/convert/test/45');
        $this->assertStatus($response, self::STATUS_OK);
        $this->assertEquals('50', $response->getContent());
    }
}
