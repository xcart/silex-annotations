<?php

/**
 * Copyright (c) 2001-present X-Cart Holdings LLC. All rights reserved.
 * See https://www.x-cart.com/license-agreement.html for license details.
 */

namespace XCart\SilexAnnotationsTest\Fixtures\ServiceAnnotationSample;

use XCart\SilexAnnotations\Annotations\Service;

/**
 * @Service\Service(arguments={"service"="x_cart.silex_annotations_test.fixtures.service_annotation_sample.mapping_service"})
 */
class WithMappingService {
    public $service;
    public function __construct(MappedService $service)
    {
        $this->service = $service;
    }
}
